const FIRMA = 'Generali';
const ULICE = 'Na Pankráci';
const JMENO_A_PRIJMENI = 'Vilém Lipold';

let cisloPopisne = 123;

// let veta = 'Pracuji ve firmě ' + FIRMA + ' na adrese ' + ULICE + ' ' + cisloPopisne + '. Jmenuji se ' + JMENO_A_PRIJMENI;

let veta = `Pracuji ve firmě ${FIRMA} na adrese ${ULICE} ${cisloPopisne}. Jmenuji se ${JMENO_A_PRIJMENI}.`;

document.write(veta);

cisloPopisne = cisloPopisne + 20;

veta = `Vchod o deset domů má číslo ${cisloPopisne}.`;

document.write('<br>');
document.write(veta);

const DOTAZ = 'Zadejte svůj věk.';
const DEFAULTNI_VEK = 18;

const VEK = prompt(DOTAZ, DEFAULTNI_VEK);

document.write('<br>');
document.write(VEK + '/' + typeof VEK);

const ARE_YOU_GOTT = confirm('Jste Karel Gott?');
document.write('<br>');
document.write(ARE_YOU_GOTT + '/' + typeof ARE_YOU_GOTT);

/*
document.write('<br>');
document.write(DEFAULTNI_VEK === VEK);
*/
document.write('<br>');

let kam = 'Jdi na procházku.';
if (VEK > 65) {
  kam = 'Jdi do parku.';
} else if (VEK >= DEFAULTNI_VEK) {
  kam = 'Můžeš do hospody.';
}

document.write(kam);
document.write('<br>');

/*
let wakeUp;
if (ARE_YOU_GOTT) {
  wakeUp = 'z hrobu';
} else {
  wakeUp = 'z postele';
}
*/

const wakeUp = ARE_YOU_GOTT ? 'z hrobu' : 'z postele';

document.write(`Vstal jsi ${wakeUp}.`);

const pole = [1, 2, 3, 4, 5, 6];
pole.push(7);

let i = 0;

while (pole[i] !== undefined) {
  console.log(pole[i]);
  i++;
}

let jmeno;

do {
  jmeno = prompt('Zadejte vaše jméno');
  jmeno = !!jmeno && jmeno.trim();
  /*
  if (jmeno) {
    jmeno = jmeno.trim();
  }
  */
} while(!jmeno)

console.log(jmeno);
