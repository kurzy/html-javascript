const actions = [
  'koukat na bednu',
  'mazlit se s manželkou',
  'mazlit se psem',
  'půjdu do hospody',
  'půjdu do hospody',
  'půjdu do divadla',
  'romantika s manželkou',
  'romantika s milenkou',
];

const button = document.getElementById('hodButton');
const output = document.getElementById('vystup');

function getRandomAction() {
  const index = Math.floor(Math.random() * actions.length);
  const action = actions[index];
  return action;
}

function roll() {
  const action = getRandomAction();
  output.textContent = action;
}

button.addEventListener('click', roll);
